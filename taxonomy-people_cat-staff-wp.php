<?php get_header(); 

            // vars
            $queried_object = get_queried_object(); 
            $taxonomy = $queried_object->taxonomy;
            $term_id = $queried_object->term_id;
            $term_slug = $queried_object->slug;
            // echo $term_slug;
?>
			<div class="content main" id="main-content">
				<header>
					<h1><?php single_cat_title(); ?></h1>
					<?php $category_description = category_description();
					if ( ! empty( $category_description ) )
					echo apply_filters( 'category_archive_meta', '<p>' . $category_description . '</p>' );
					?>
				</header>
				<div class="people-list">
					<ul <?php post_class('cf'); ?>>
					<?php $staff_loop = new WP_Query( array( 'people_cat' => $term_slug, 'post_type' => 'people', 'posts_per_page' => -1, 'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC') ); ?>
					<?php while ( $staff_loop->have_posts() ) : $staff_loop->the_post(); ?>
						<li class="person-item">
                            <?php if ( have_posts() ) : while ( have_posts() ) : ?>
							 <a href="<?php the_permalink() ?>">
                            <?php endif; ?>
							<dl>
								<dt class="name"><?php the_title(); ?></dt>									
								<dd class="position"><?php the_field('position_title'); ?></dd>
								<?php if(get_field('office')) { ?>
									<dd class="office"><strong>Office: </strong><?php the_field('office'); ?></dd>
								<?php } ?>
								<?php if(get_field('office_hours')) { ?>
									<dd><strong>Office Hours: </strong><?php the_field('office_hours'); ?></dd>
								<?php } ?>
								<?php if(get_field('phone_number')) { ?>
									<dd class="phone"><strong>Phone: </strong><?php the_field('phone_number'); ?></dd>
								<?php } ?>                                
								<?php if(get_field('email_address')) { ?>
								<dd class="email">
									<a href="mailto:<?php the_field('email_address'); ?>"><?php the_field('email_address'); ?></a>
								</dd>
								<?php } ?>
							</dl>
                            <?php if ( have_posts() ) : while ( have_posts() ) : ?>
							 </a>
                            <?php endif; ?>                          
						</li>
					<?php endwhile; ?>					
					</ul>
				</div>
			</div>
<?php get_footer(); ?>