<div class="col appt-col one">
    <?php $home_loop = new WP_Query( array( 'pagename' => 'wc') ); ?>
    <?php while ( $home_loop->have_posts() ) : $home_loop->the_post(); ?>
        <h3>Make an Appointment</h3>

        <?php if((get_sub_field('appointment_url')) && (get_sub_field('button_label'))){ ?>
            <img src="<?php echo get_template_directory_uri(); ?>/library/images/appt_cal_icon.png" class="bookit-icon" />
            <a href="<?php the_sub_field('appointment_url'); ?>" class="bookit btn">                               
                <?php the_sub_field('button_label'); ?>                       
            </a>
        <?php } ?>

        <?php if(get_sub_field('appointment_url')){ ?>                        
            <a href="<?php the_sub_field('appointment_url'); ?>">
        <?php } ?>
        <?php if(get_sub_field('appointment_title')){ ?>
            <h4><?php the_sub_field('appointment_title'); ?></h4>
        <?php } ?>
        <?php if(get_sub_field('appointment_url')){ ?>                        
            </a>
        <?php } ?>
        <?php if(get_sub_field('appointment_description')){ ?>
            <span class="bookit-descript"><?php the_sub_field('appointment_description'); ?></span>
        <?php } ?>
    <?php endwhile; ?>
</div>