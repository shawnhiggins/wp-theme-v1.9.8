<?php         
    // CODE to help figure out which section is displaying
       // This check the url and make sure the elements switch..
    $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    $switchURL = 'wp';
    if ((strpos($url,'uwc') !== false) ||(strpos($url,'wc') !== false)) {

        $switchURL = 'uwc';
    } elseif (strpos($url,'esl') !== false) {
        $switchURL = 'esl';
    } else {
        $switchURL = 'wp';
    }
?>
<div class="col menu-col one">
	<h3><?php the_sub_field('menu_title'); ?></h3>
	<?php wp_nav_menu(array(
		'container' => '',
        'menu' => __( $switchURL.'Quick Links', 'bonestheme' ),
        'menu_class' => $switchURL.'-quick-links',
        'theme_location' => $switchURL.'-quick-links',
        'before' => '',
        'after' => '',
        'link_before' => '<h4>',
        'link_after' => '</h4>',
        'depth' => 0,
	)); ?>
</div>