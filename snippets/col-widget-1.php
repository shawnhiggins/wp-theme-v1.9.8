<div class="col widget-col <?php the_sub_field('widget_width'); ?>">
	<h3><?php the_sub_field('widget_title'); ?></h3>
	<?php if ( is_active_sidebar( 'widget-1-sidebar' ) ) : ?>
		<?php dynamic_sidebar( 'widget-1-sidebar' ); ?>
	<?php else : endif; ?>
</div>