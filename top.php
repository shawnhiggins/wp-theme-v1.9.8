<?php
/*
	Basic Navigation and Hero 
*/

      
    // CODE to help figure out which section is displaying
       // This check the url and make sure the elements switch..
    $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    $switchURL = 'wp';
    if ((strpos($url,'uwc') !== false) ||(strpos($url,'wc') !== false)) {

        $switchURL = 'uwc';
    } elseif (strpos($url,'esl') !== false) {
        $switchURL = 'esl';
    } else {
        $switchURL = 'wp';
    }
?>

<a href="#main-content" class="hidden skip">Skip to main content</a>
<div id="container">
	<header role="banner" class="top">
		<div class="content">
            <div class="icon-menu">
                <?php $home_loop = new WP_Query( array( 'pagename' => 'home') ); ?>
                <?php while ( $home_loop->have_posts() ) : $home_loop->the_post(); ?>
                <?php if( have_rows('menu_items') ): ?>
                        <?php while( have_rows('menu_items') ): the_row(); ?>
                        <?php
                            $slider_description = get_sub_field('title_acronym');
                            $slider_icon = get_sub_field('top_icon');

                            if( !empty($slider_icon) ): 
                                // vars
                                $url = $slider_icon['url'];
                                $title = $slider_icon['title'];
                                $slider_link = get_sub_field('link');
                                // thumbnail
                                $size = 'small-icons';
                                $icon = $slider_icon['sizes'][ $size ];
                                $width = $slider_icon['sizes'][ $size . '-width' ];
                                $height = $slider_icon['sizes'][ $size . '-height' ];
                            endif;
                        ?>
                <div class="icon">                            
                    <a href="<?php echo $slider_link; ?>" class="icon-link">                       
                        <?php if( $icon ): ?><img src="<?php echo $icon; ?>" />
                        <?php endif; ?>
                        <?php if( $slider_description ): ?>
                          <p><?php echo $slider_description; ?></p>
                        <?php endif; ?>
                    </a>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
                <?php endwhile; ?>

                <?php if(get_field('enable_donation', 'option') == "enable") { ?>
            <div class="give-back">
                <?php if(get_field('link_type', 'option') == "internal") { ?>
                <a href="<?php the_field('donation_page', 'option'); ?>" class="give">
                <?php }?>
                <?php if(get_field('link_type', 'option') == "external") { ?>
                <a href="<?php the_field('donation_link', 'option'); ?>" class="give" target="_blank">
                <?php }?>
                    <img src="<?php echo get_template_directory_uri(); ?>/library/images/circle-heart-blk.png" />
                    <p><?php the_field('button_text', 'option'); ?></p></a>
            </div>
            <?php }?>
            <?php if($switchURL == 'uwc'){ ?>                        
                <div class="icon">                         
                <?php $home_loop = new WP_Query( array( 'pagename' => 'wc') ); ?>
                <?php while ( $home_loop->have_posts() ) : $home_loop->the_post(); ?>                                                    
                        <a href="<?php the_field('appointment_url'); ?>" class="icon-link">

                        <img src="<?php echo get_template_directory_uri(); ?>/library/images/bookit_top_icon_blk.png" alt="Book it! Link"  />
                            <?php if(get_field('button_label')){ ?>
                                <p>Book</p>
                            <?php } ?>
                        </a>
                    <?php endwhile; ?>
                   </div>
                <?php } ?>
            </div>
                    <?php if($switchURL !== 'uwc'){ ?>
				        <a href="<?php echo home_url(); ?>/<?php echo $switchURL; ?>" rel="nofollow">
                    <?php }else{ ?>                               
				        <a href="<?php echo home_url(); ?>/wc" rel="nofollow">
                    <?php } ?>
						<img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-logo.svg" alt="UCLA" class="university-logo" /><span class="hidden">UCLA</span>
				    <div class="dept-logo"> 
                    <?php // H1 if homepage, H2 otherwise.
                        if ( is_front_page() ) { ?>
						<img src="<?php echo get_template_directory_uri(); ?>/library/images/<?php echo $switchURL; ?>-dept-logo.png" alt="<?php the_field('department_name', 'option'); ?>" class="dept-logo" /><span class="hidden"><?php the_field('department_name', 'option'); ?></span>
                    <?php } else { ?>
						<img src="<?php echo get_template_directory_uri(); ?>/library/images/<?php echo $switchURL; ?>-dept-logo.png" alt="<?php the_field('department_name', 'option'); ?>" class="dept-logo" /><span class="hidden"><?php the_field('department_name', 'option'); ?></span>
                    <?php }?>
                        </div>
                       </a>               
                
		</div>
            <?php if ( !is_front_page() ) : ?>
                <div class="nav-search">
                    <div class="content">
                        <nav role="navigation" aria-labelledby="main navigation" class="desktop">
                            <?php wp_nav_menu(array(
                                'container' => false,
                                'menu' => __( $switchURL.' Main Menu', 'bonestheme' ),
                                'menu_class' => $switchURL.'-main-nav',
                                'theme_location' => $switchURL.'-main-nav',
                                'before' => '',
                                'after' => '',
                                'depth' => 2,
                            )); ?>
                        </nav>
                        <?php get_search_form(); ?>      
                    </div>            
                </div>
                <?php endif; ?>
                    <nav role="navigation" aria-labelledby="main navigation" class="mobile">
                        <?php wp_nav_menu(array(
                            'container' => false,
                            'menu' => __( $switchURL.' Main Menu', 'bonestheme' ),
                            'theme_location' => $switchURL.'-main-nav',
                            'before' => '',
                            'after' => '',
                            'depth' => 2,
                            'items_wrap' => '<ul id="mobile-nav">%3$s</ul>'
                        )); ?>
                    </nav>
	</header>
    <!--// added spotlight by shawn -->
    <?php get_template_part( 'section-menu' ); ?>
	<?php 
		// Don't do any of the below if homepage
		if ( is_front_page() ) { }
		// Breadcrumb everywhere else
		elseif ( is_single() || is_category( $category ) || is_archive() ) { 
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('<div class="breadcrumbs"><div class="content">','</div></div>');
			}
		}
		else {
			// Only show hero image on a page or post
			if ( has_post_thumbnail() && is_single() || has_post_thumbnail() && is_page() ) {
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'hero' );
				$url = $thumb['0']; ?>
	<div id="hero" style="background-image: url('<?=$url?>');">
		&nbsp;
	</div>
	<?php }
			// And show breadcrumb
            if ( function_exists('yoast_breadcrumb') ) {
                // To help make sure breadcrumbs don't show on internal home pages.
                if(!is_page_template( 'page-home.php' ) ) {
                yoast_breadcrumb('<div class="breadcrumbs"><div class="content">','</div></div>');
                }
            } 
        } 
	?>