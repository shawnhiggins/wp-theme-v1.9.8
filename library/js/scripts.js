/********************************

Table of Contents

1. Sticky menus
2. Open files in new window
3. Smooth scrolling effect when using anchor links
4. Google Map embed with ACF
5. People Page Filter
6. Accessible Dropdown Menus
7. Fancybox
8. Accessible Hide/Show

*/

jQuery("document").ready(function($) {
	
/********************************
	1. Sticky menus
*/

// Enter the class of the block you want to stick.
// http://leafo.net/sticky-kit/

//	$(".nav-container").stick_in_parent()
    
/********************************
 * Initiallizing mobile menu
*/
	$('#mobile-nav').slicknav({
		duplicate: false,
		allowParentLinks: true,
		label: "",
		prependTo: '.mobile'
	});

/********************************
	2. Open files in new window
*/

	$('a[href$=".pdf"]').attr('target', '_blank');
	$('a[href$=".doc"]').attr('target', '_blank');
	$('a[href$=".docx"]').attr('target', '_blank');
	$('a[href$=".xls"]').attr('target', '_blank');
	$('a[href$=".xlsx"]').attr('target', '_blank');

/********************************
	3. Smooth scrolling effect when using anchor links
*/

	$(function() {
		$('a[href*=#]:not([href=#])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
		});
	});

/********************************
	4. Google Map embed with ACF
	https://www.advancedcustomfields.com/resources/google-map/
*/

	// This function will render a Google Map onto the selected jQuery element
	function new_map( $el ) {
		// var
		var $markers = $el.find('.marker');
		// vars
		var args = {
			zoom		: 16,
			center		: new google.maps.LatLng(0, 0),
			mapTypeId	: google.maps.MapTypeId.ROADMAP,
			scrollwheel	: false
		};
		// create map	        	
		var map = new google.maps.Map( $el[0], args);
		// add a markers reference
		map.markers = [];
		// add markers
		$markers.each(function(){
	    	add_marker( $(this), map );
		});
		// center map
		center_map( map );
		// return
		return map;
	}
	
	// This function will add a marker to the selected Google Map	
	function add_marker( $marker, map ) {
		// var
		var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );
		// create marker
		var marker = new google.maps.Marker({
			position	: latlng,
			map			: map
		});
		// add to array
		map.markers.push( marker );
		// if marker contains HTML, add it to an infoWindow
		if( $marker.html() ) {
			// create info window
			var infowindow = new google.maps.InfoWindow({
				content		: $marker.html()
			});
			// show info window when marker is clicked
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open( map, marker );
			});
		}
	}
	
	// This function will center the map, showing all markers attached to this map
	function center_map( map ) {
		// vars
		var bounds = new google.maps.LatLngBounds();
		// loop through all markers and create bounds
		$.each( map.markers, function( i, marker ){
			var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
			bounds.extend( latlng );
		});
		// only 1 marker?
		if( map.markers.length == 1 ) {
			// set center of map
		    map.setCenter( bounds.getCenter() );
		    map.setZoom( 16 );
		}
		else
		{
			// fit to bounds
			map.fitBounds( bounds );
		}
	}

	// This function will render each map when the document is ready (page has loaded)
	// global var
	var map = null;	
	$('.map').each(function(){
		// create map
		map = new_map( $(this) );
	});


/********************************
	5. People Page Filter
*/

	//@prepros-append libs/isotope.pkgd.min.js
	
	// init Isotope
	var $container = $('.people-list').isotope({
		itemSelector: '.person-item'
	});
	/*
	Uncomment to change default filter from view all.
	Be sure to edit the People Listing file to remove the "is-checked" class from View All, 
	and to change the default filter title.
	Within Wordpress, add "is-checked" to the desired default menu item .
	*/
	/*	
	// Change filter to class that you want to be the default
	$container.isotope({
		filter: '.football'
	});
	*/
	
	// store filter for each group
	var filters = {};

	$('.filter').on( 'click', '.option', function() {
		var $this = $(this);
		// get group key
		var $buttonGroup = $this.parents('.button-group');
		var filterGroup = $buttonGroup.attr('data-filter-group');
		// set filter for group
		filters[ filterGroup ] = $this.attr('data-filter');
		// combine filters
		var filterValue = concatValues( filters );
		// set filter for Isotope
		$container.isotope({ filter: filterValue });
	});

	// change is-checked class on buttons
	$('.button-group').each( function( i, buttonGroup ) {
		var $buttonGroup = $( buttonGroup );
		$buttonGroup.on( 'click', 'button', function() {
			$buttonGroup.find('.is-checked').removeClass('is-checked');
			$( this ).addClass('is-checked');
		});
	});

	// flatten object by concatting values
	function concatValues( obj ) {
		var value = '';
		for ( var prop in obj ) {
			value += obj[ prop ];
		}
		return value;
	}
	
	// Change page title on click
	$('.option').click(function () {
		$('.filter-title').text($(this).data("text"));
	});

	// Filter for mobile
	var $container = $('.people-list'),
	$select = $('.dropdown-filter select');
	
	$container.isotope({
		itemSelector: '.person-item'
	});
	
	$select.change(function() {
		var filters = $(this).val();
		$container.isotope({
			filter: filters
		});
	});

/********************************
	6. Accessible Dropdown Menus
*/
	
	//@prepros-append libs/accessible-menu.min.js
	
	(function ($) {
		"use strict";
		$(document).ready(function () {
			// initialize the megamenu
			$('.megamenu').accessibleMegaMenu();
			// hack so that the megamenu doesn't show flash of css animation after the page loads.
			setTimeout(function () {
				$('body').removeClass('init');
			}, 500);
		});
	}
	(jQuery));
	
	// Finds the first nav menu on the page
	$("nav:first").accessibleMegaMenu({
		/* prefix for generated unique id attributes, which are required 
		to indicate aria-owns, aria-controls and aria-labelledby */
		uuidPrefix: "accessible-menu",
		
		// css class used to define the megamenu styling
		menuClass: "main-nav",
		
		// css class for a top-level navigation item in the megamenu
		topNavItemClass: "parent-item",
		
		// css class for a megamenu panel
		panelClass: "sub-menu",
		
		// css class for a group of items within a megamenu panel
		panelGroupClass: "sub-menu",
		
		// css class for the hover state
		hoverClass: "hover",
		
		// css class for the focus state
		focusClass: "focus",
		
		// css class for the open state
		openClass: "open"
	});

/********************************
	7. Fancybox
*/

	$("[data-fancybox]").fancybox({
		//toolbar : false,
		//infobar : false,
		buttons : [
		       //'slideShow',
		       //'fullScreen',
		       //'thumbs',
		       //'share',
		       //'download',
		       //'zoom',
		       'close'
		],
		margin : [60, 0],
	});

/********************************
 * End document ready function
*/

});

/********************************
	8. Accessible Hide/Show
*/

//@prepros-append libs/accessible-hide-show.js