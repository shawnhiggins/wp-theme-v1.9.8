<?php get_header(); 

            // vars
            $queried_object = get_queried_object(); 
            $taxonomy = $queried_object->taxonomy;
            $term_id = $queried_object->term_id;
            $term_slug = $queried_object->slug;
?>
			<div class="content main" id="main-content">
				<header>
					<h1><?php single_cat_title(); ?></h1>
					<?php $category_description = category_description();
					if ( ! empty( $category_description ) )
					echo apply_filters( 'category_archive_meta', '<p>' . $category_description . '</p>' );
					?>
					<?php if ( has_nav_menu( 'faculty-filter' ) ) {?> 
					<div class="filter">
					<?php // To make another filter, duplicate the div below ?>
						<div class="options button-group" data-filter-group="field">
							<h3>Field of Study</h3>
							<ul>
								<button data-filter="" class="option all is-checked">View All</button>
								<?php wp_nav_menu(array(
									'container' => false,
									'menu' => __( 'Faculty Filter', 'bonestheme' ),
									'menu_class' => 'faculty-filter',
									'theme_location' => 'faculty-filter',
									'before' => '',
									'after' => '',
									'depth' => 1,
									'items_wrap' => '%3$s',
									'walker' => new Filter_Walker
								)); ?>
							</ul>
						</div>
					</div>
					<?php if( have_rows('filters', 'option') ): ?>
					<script type="text/javascript">
						jQuery("document").ready(function($) {
							$('.all').click(function() {
								$('.filter-title').html('All');
							});
							<?php while( have_rows('filters', 'option') ): the_row();
							// vars
								$class = get_sub_field('class');
								$category = get_sub_field('category');
							?>
							$('.filter .<?php echo $class ?>').click(function() {
								$('.filter-title').html('<?php echo $category->name; ?>');
							});						
							<?php endwhile; ?>
						});
					</script>
					<?php endif; ?>					
					<h2 class="filter-title">All</h2>
					<?php } ?> 
				</header>
				<div class="people-list">
					<ul <?php post_class('cf'); ?>>
					<?php $ta_loop = new WP_Query( array( 'people_cat' => $term_slug, 'post_type' => 'people', 'posts_per_page' => -1, 'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC')); ?>
					<?php while ( $ta_loop->have_posts() ) : $ta_loop->the_post(); ?>
						<li class="person-item">
							<dl>
								<dt class="name"><?php the_title(); ?></dt>									
								<dd class="position"><?php the_field('position_title'); ?></dd>
								<?php if(get_field('office')) { ?>
									<dd class="office"><strong>Office: </strong><?php the_field('office'); ?></dd>
								<?php } ?>
								<?php if(get_field('office_hours')) { ?>
									<dd><strong>Office Hours: </strong><?php the_field('office_hours'); ?></dd>
								<?php } ?>
								<?php if(get_field('phone_number')) { ?>
									<dd class="phone"><strong>Phone: </strong><?php the_field('phone_number'); ?></dd>
								<?php } ?>                                
								<?php if(get_field('email_address')) { ?>
								<dd class="email">
									<a href="mailto:<?php the_field('email_address'); ?>"><?php the_field('email_address'); ?></a>
								</dd>
								<?php } ?>
							</dl>                          
						</li>
					<?php endwhile; ?>					
					</ul>
				</div>
			</div>
<?php get_footer(); ?>