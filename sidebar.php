<?php         
    // CODE to help figure out which section is displaying
       // This check the url and make sure the elements switch..
    $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    $switchURL = 'wp';
    if ((strpos($url,'uwc') !== false) ||(strpos($url,'wc') !== false)) {

        $switchURL = 'uwc';
    } elseif (strpos($url,'esl') !== false) {
        $switchURL = 'esl';
    } else {
        $switchURL = 'wp';
    }
?>
<?php // For Event month or list landing page, https://gist.github.com/jo-snips/2415009
				// Only run if The Events Calendar is installed 
				if ( tribe_is_past() || tribe_is_upcoming() && !is_tax() || tribe_is_month() && !is_tax() || tribe_is_upcoming() && is_tax()) { 
					// Do nothing	
				}
				// For posts
				elseif (is_single() || is_category() || is_search()) { ?>
				<div class="col side feed" role="complementary">
					<?php if ( is_active_sidebar( 'news-sidebar' ) ) :  ?>
					<?php dynamic_sidebar( 'news-sidebar' ); ?>
					<?php else : endif; ?>
					<?php if ( is_active_sidebar( 'events-sidebar' ) ) :  ?>
					<?php dynamic_sidebar( 'events-sidebar' ); ?>
					<?php else : endif; ?>
				</div>
				<?php } ?>
				<?php // For pages
				if (is_page() || is_404()) { ?>
				<div class="col side">
					<div class="content">
						<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php
								// If a Students subpage
								if (is_tree(1343) || get_field('menu_select') == "students") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Students', 'bonestheme' ),
										'menu_class' => 'students-nav',
										'theme_location' => 'students-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>Students</h3> <ul>%3$s</ul>'
									));
								}
								// If a Outreach subpage 
								if (is_tree(1323) || is_tree(1338) || get_field('menu_select') == "outreach") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Outreach', 'bonestheme' ),
										'menu_class' => 'outreach-nav',
										'theme_location' => 'outreach-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>Outreach</h3> <ul>%3$s</ul>'
									));
								}
								// If a Writing Resources subpage
								if (is_tree(939) || get_field('menu_select') == "writing") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Writing', 'bonestheme' ),
										'menu_class' => 'writing-nav',
										'theme_location' => 'writing-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>Writing Resources</h3> <ul>%3$s</ul>'
									));
								}
								// If a UCLA Writes! subpage
								if (is_tree(1350) || get_field('menu_select') == "uclawrites") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Ucla Writes!', 'bonestheme' ),
										'menu_class' => 'uclawrites-nav',
										'theme_location' => 'uclawrites-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>UCLA Writes!</h3> <ul>%3$s</ul>'
									));
								}                        
								// If a About subpage
								if (is_tree(8630) || get_field('menu_select') == "about") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'About', 'bonestheme' ),
										'menu_class' => 'about-nav',
										'theme_location' => 'about-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>About</h3> <ul>%3$s</ul>'
									));
								}
								// If a Graduate subpage
								if (is_tree(1863) || get_field('menu_select') == "graduate") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Graduate', 'bonestheme' ),
										'menu_class' => 'graduate-nav',
										'theme_location' => 'graduate-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>Graduate</h3> <ul>%3$s</ul>'
									));
								}
								// If an Undergraduate subpage
								if (is_tree(860) || get_field('menu_select') == "undergraduate") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Undergraduate', 'bonestheme' ),
									   	'menu_class' => 'undergrad-nav',
									   	'theme_location' => 'undergrad-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Undergraduate</h3> <ul>%3$s</ul>'
									));
								}
								// If an UWC About subpage
								if (is_tree(1694) || get_field('menu_select') == "aboutuwc") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'About UWC', 'bonestheme' ),
									   	'menu_class' => 'aboutuwc-nav',
									   	'theme_location' => 'aboutuwc-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>About UWC</h3> <ul>%3$s</ul>'
									));
								}
								// If a UWC For Students subpage
								if (is_tree(1704) || get_field('menu_select') == "forstudents") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'For Students', 'bonestheme' ),
										'menu_class' => 'forstudents-nav',
										'theme_location' => 'forstudents-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>For Students</h3> <ul>%3$s</ul>'
									));
								}
								// If an ESL General Info subpage
								if (is_tree(1400) || get_field('menu_select') == "general-esl") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'ESL General Info', 'bonestheme' ),
									   	'menu_class' => 'esl-general-nav',
									   	'theme_location' => 'esl-general-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>General Information</h3> <ul>%3$s</ul>'
									));
								}
								// If an ESL Courses subpage
								if (is_tree(1214) || get_field('menu_select') == "courses-esl") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'ESL Courses', 'bonestheme' ),
									   	'menu_class' => 'esl-courses-nav',
									   	'theme_location' => 'esl-courses-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Courses</h3> <ul>%3$s</ul>'
									));
								}
								// If an ESL Placement subpage
								if (is_tree(1419) || get_field('menu_select') == "placement-esl") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'ESL Placement', 'bonestheme' ),
									   	'menu_class' => 'esl-placement-nav',
									   	'theme_location' => 'esl-placement-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Placement</h3> <ul>%3$s</ul>'
									));
								}
								// If an ESL Faqs subpage
								if (is_tree(1432) || get_field('menu_select') == "faq-esl") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'ESL General Info', 'bonestheme' ),
									   	'menu_class' => 'esl-faq-nav',
									   	'theme_location' => 'esl-faq-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>FAQs</h3> <ul>%3$s</ul>'
									));
								}
								// For Search, 404's, or other pages you want to use it on
								// Replace 9999 with id of parent page
								if (is_tree(9999) || is_search() || is_404() || is_page('contact') || is_page('about') || get_field('menu_select') == "general") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Main Menu', 'bonestheme' ),
										'menu_class' => 'side-nav',
										'theme_location' => $switchURL.'-main-nav',
										'before' => '',
										'after' => '',
										'depth' => 1,
										'items_wrap' => '<h3>Main Menu</h3> <ul>%3$s</ul>'
									));
								}
							?>
						</nav>
					</div>
				</div>
				<?php } ?>